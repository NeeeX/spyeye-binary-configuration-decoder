#include <stdio.h>
#include <windows.h>

/*
21.10.2011 - 1.0
*/

enum
{
	MODE_DEC_CONFIG,
	MODE_EXDEC_FROM_BINARY,
};

enum
{
	ERROR_EMPTYORNO_PARAMETERS,
	ERROR_CONFIG_NOT_FOUND,
	ERROR_READ_CONFIG,
	ERROR_WRITE_OUTPUT
};

bool fileExists(LPWSTR fileName)
{
	if (GetFileAttributes(fileName) == 0xFFFFFFFF)
		return false;
	else
		return true;
}

LPBYTE readFileToMem(LPWSTR fileName, LPDWORD bytesRead)
{
	DWORD  fileSize = 0;
	HANDLE hFile = CreateFile(fileName, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);

	if (hFile && hFile != INVALID_HANDLE_VALUE)
	{
		fileSize = GetFileSize(hFile, 0);
		LPBYTE fileBuffer = new BYTE [fileSize];

		if (fileBuffer)
		{
			if (ReadFile(hFile, fileBuffer, fileSize, bytesRead, 0))
			{
				CloseHandle(hFile);
				return fileBuffer;
			}
		}
	}

	CloseHandle(hFile);

	return 0;
}

LPVOID readFileResource(LPWSTR fileName, LPCWSTR resName, LPCWSTR subResName, LPDWORD resSize)
{
	HMODULE hFile = LoadLibraryEx(fileName, 0, LOAD_LIBRARY_AS_DATAFILE);

	if (hFile)
	{
		HRSRC hRes = FindResource(hFile, subResName, resName);

		if (hRes)
		{
			HANDLE hGlob = LoadResource(hFile, hRes);
			*resSize	 = SizeofResource(hFile, hRes);

			if (hGlob && *resSize > 0)
			{
				LPVOID lpData = new BYTE [*resSize];

				if (lpData)
				{
					memcpy(lpData, hGlob, *resSize);
					return lpData;
				}
			}
		}

	}

	return NULL;
}

BOOL writeMemToFile(LPWSTR fileName, LPBYTE data, DWORD size, LPDWORD bytesWritten)
{
	HANDLE hFile = CreateFile(fileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);

	if (hFile && hFile != INVALID_HANDLE_VALUE)
	{
		if (WriteFile(hFile, data, size, bytesWritten, 0))
		{
			CloseHandle(hFile);
			return true;
		}
	}

	CloseHandle(hFile);
	return false;
}


void configDecrypt(LPBYTE data, DWORD size)
{
	BYTE b;
	for (unsigned int i = size; i >= 1; i-- )
	{
		b = data[i] ^ 0x4C;
		b = b - data[i - 1];
		data[i] =  b;
	}
}

void displayError(DWORD errorCode)
{
	LPWSTR szErrorText = NULL;

	switch (errorCode)
	{
		case ERROR_EMPTYORNO_PARAMETERS:
			szErrorText = L"Usage: decrypt.exe <config|binary> file_encrypted config_decrypted.zip";
			break;
		case ERROR_CONFIG_NOT_FOUND:
			szErrorText = L"Config file not found.";
			break;
		case ERROR_READ_CONFIG:
			szErrorText = L"Couldn't load config file to memory.";
			break;
		case ERROR_WRITE_OUTPUT:
			szErrorText = L"Couldn't write decoded config file to disk.";
			break;
		default:
			szErrorText = L"Unknown.";
			break;
	}

	wprintf(L"Error: %s", szErrorText);
}


void displayBanner()
{
	printf("SpyEye 1.3.x config decoder \n\n");
}

void displaySuccess()
{
	printf("Sucessfully decoded config file.");
}

int wmain(int argc, WCHAR* argv[])
{
	DWORD  optAction	= 1;
	DWORD  configWrote	= 0;
	DWORD  configRead	= 0;
	DWORD  configKeyRead=0;
	LPBYTE configBuffer = {0};
	LPSTR  configKeyA	= 0;
	LPWSTR configKeyW	= 0;
	LPWSTR tmpPointer	= 0;
	LPWSTR decryptMode  = argv[1];
	LPWSTR configName	= argv[2];
	LPWSTR outputName	= argv[3];

	displayBanner();


	if (decryptMode == 0 || configName == 0 || outputName == 0)
	{
		displayError(ERROR_EMPTYORNO_PARAMETERS);
		return 0;
	}

	if (fileExists(configName) == FALSE)
	{
		displayError(ERROR_CONFIG_NOT_FOUND);
		return 0;
	}

	if (wcscmp(decryptMode, L"binary") == 0)
	{
		optAction	 = MODE_EXDEC_FROM_BINARY;
		configBuffer = (LPBYTE)readFileResource(configName, L"C", L"C2", &configRead);
	}
	else
	{
		optAction	 = MODE_DEC_CONFIG;
		configBuffer = readFileToMem(configName, &configRead);
	}

	if (configBuffer == 0)
	{
		displayError(ERROR_READ_CONFIG);
		return 0;
	}


	configDecrypt(configBuffer, configRead);

	if (optAction == MODE_EXDEC_FROM_BINARY)
	{
		configKeyA = (LPSTR)readFileResource(configName, L"C", L"C3", &configKeyRead);

		if (configKeyA != NULL)
		{
			configKeyW = new WCHAR [(strlen(configKeyA) + 1 )];

			if (configKeyW)
			{
				MultiByteToWideChar(CP_UTF8, 0, configKeyA, strlen(configKeyA)+1, configKeyW, (strlen(configKeyA) + 1 ) * 2);
				wsprintf(outputName, L"decrypted_%s.zip", configKeyW);
				delete [] configKeyW;
			}
		}
	}

	if (!writeMemToFile(outputName, configBuffer, configRead, &configWrote))
	{
		displayError(ERROR_WRITE_OUTPUT);
		return 0;
	}
	else
	{
		delete [] configBuffer;
	}

	displaySuccess();
}